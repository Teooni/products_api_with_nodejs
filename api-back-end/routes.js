let clientController = require("./src/controllers/clientController");
let produsController = require("./src/controllers/produsController");
let favoriteController = require("./src/controllers/favoriteController");

module.exports = function (app) {
  // Routers
  //based on methods defined in Controllers

  //CLIENT
  app.post('/login', clientController.login)
  app.get("/clients", clientController.select); //get all the clients
  app.get("/clients/:id", clientController.selectOne); //get one client
  app.post("/clients", clientController.add); //add a new client
  app.put("/clients/:id", clientController.update); //update a client
  app.delete("/clients/:id", clientController.delete); //delete a client

  // PRODUS
  app.get("/products", produsController.select); //get all products
  app.get("/products/:id", produsController.selectOne);//get one product
  app.post("/products", produsController.add); //add a new product
  app.put("/products/:id", produsController.update); //update a product
  app.delete("/products/:id", produsController.delete); //delete a product

  // FAVORITES
  app.get("/favorites/:id", favoriteController.select); //get the list with favourites
  app.post("/favorites", favoriteController.add); //add a record to the favourites list
  app.delete("/favorites/:idclient/:idprodus", favoriteController.delete); //delete a record from favourites list

};
