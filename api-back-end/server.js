// Import express
let express = require('express');
const path = require('path');
const cors = require("cors");
const db = require('./models') // import models for sequelize

const HOST = process.env.HOST || '127.0.0.1';
const PORT = process.env.PORT || 3003;

// Initialize the app
let app = express();
var corsOptions = {
    origin: 'http://proiect-api-demo.com:3000',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

app.use(cors(corsOptions));


// used to get data in json format from body
app.use(express.json())
require('./routes')(app);

// This creates the table if it doesn't exist (and does nothing if it already exists)

// db.sequelize.sync().then(() => {
//     app.listen(PORT, HOST, function () {
//         console.log(`Server run on:  http://${HOST}:${PORT}/`);
//     });
// });

//This creates the table, dropping it first if it already existed

// db.sequelize.sync({ force: true }).then(() => {
//     app.listen(PORT, HOST, function () {
//         console.log(`Server run on:  http://${HOST}:${PORT}/`);
//     });
// });

// This checks what is the current state of the table in the database (which columns
// it has, what are their data types, etc), and then performs the necessary changes in
// the table to make it match the model.

// db.sequelize.sync({ alter: true }).then(() => {
//     app.listen(PORT, HOST, function () {
//         console.log(`Server run on:  http://${HOST}:${PORT}/`);
//     });
// });


// Launch app to listen to specified port without synchronization the db
app.listen(PORT, HOST, function () {
    console.log(`Server run on:  http://${HOST}:${PORT}/`);
});
