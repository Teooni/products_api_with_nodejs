const db = require('../../models');

class AdministrateClient {
    //---------Login
    async Login(email) {
        const response = await db.clienti.findAll({
            where: {
                email: email
            }
        }).catch((err) => console.log("ERROR:", err));
        return response;
    };
    //---------Get all the clients
    async GetClients() {
        return await db.clienti.findAll().catch((err) => console.log("ERROR:", err));
    };
    //------------Get a single client
    async GetClient(id) {
        return await db.clienti.findAll({
            where: {
                idclient: id
            }
        }).catch((err) => console.log("ERROR:", err));
    }
    //---------Add a new client in DB
    async AddClient(user) {
        return await db.clienti.create({
            firstName: user.firstName,
            lastName: user.lastName,
            phone: user.phone,
            email: user.email,
            password: user.password,
            isAdmin: user.isAdmin
        }).catch((err) => console.log("ERROR:", err));
    }
    //------------Update the client's info
    async UpdateClient(user) {
        return await db.clienti.update({
            firstName: user.firstName,
            lastName: user.lastName,
            phone: user.phone,
            email: user.email,
            password: user.password,
            isAdmin: user.isAdmin
        }, {
            where: {
                idclient: user.idclient
            }
        }).catch((err) => console.log("ERROR:", err));
    }
    //--------------Delete a client
    async RemoveClient(id) {
        return await db.clienti.destroy({
            where: {
                idclient: id
            }
        }).catch((err) => console.log("ERROR:", err));
    }

};

module.exports = AdministrateClient;