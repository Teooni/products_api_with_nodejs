const db = require('../../models');

class AdministrateProdus {
    //---------Get all the products from DB
    async GetProduse() {
        return await db.produs.findAll({
            where: {
                isDeleted: 0
            }
        }).catch((err) => console.log("ERROR:", err));
    };
    //----------Get a single product
    async GetProdus(id) {
        return await db.produs.findAll({
            where: {
                idprodus: id,
                isDeleted: 0
            }
        }).catch((err) => console.log("ERROR:", err));
    }
    //-------------Add a new product in DB
    async AddProdus(produs) {
        return await db.produs.create({
            name: produs.name,
            price: produs.price
        }).catch((err) => console.log("ERROR:", err));
    }
    //---------Update a product
    async UpdateProdus(produs) {
        return await db.produs.update({
            name: produs.name,
            price: produs.price
        }, {
            where: {
                idprodus: produs.idprodus
            }
        }).catch((err) => console.log("ERROR:", err));
    }
    //------------Delete a product
    async RemoveProdus(id) {
        return await db.produs.update({
            isDeleted: 1
        }, {
            where: {
                idprodus: id
            }
        }).catch((err) => console.log("ERROR:", err));
    }
};

module.exports = AdministrateProdus;