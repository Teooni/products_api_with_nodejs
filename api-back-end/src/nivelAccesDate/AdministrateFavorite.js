const db = require('../../models');
class AdministrateFavorite {

    //----------Get all the favourites products
    async GetFavorites(idclient) {
        return await db.favourites.findAll({
            where: {
                idclient: idclient,
            },
            include: [{
                model: db.produs, // will create a left join,
                where: {
                    isDeleted: 0
                }
            }]
        }).catch((err) => console.log("ERROR:", err));
    };
    //-----------Get a favourit product-//
    async GetFavorit(idprodus, idclient) {
        return await db.favourites.findAll({
            where: {
                idclient: idclient,
                idprodus: idprodus
            }, include: [{
                model: produs // will create a left join
            }]
        }).catch((err) => console.log("ERROR:", err));
    }
    //-----------Add a product to favourties
    async AddFavorit(favourit) {
        return await db.favourites.create({
            idprodus: favourit.idprodus,
            idclient: favourit.idclient
        }).catch((err) => console.log("ERROR:", err));
    }
    //--------------Remove a product from favorites
    async RemoveFavorit(idprodus, idclient) {
        return await db.favourites.destroy({
            where: {
                idclient: idclient,
                idprodus: idprodus
            }
        }).catch((err) => console.log("ERROR:", err));
    }

};

module.exports = AdministrateFavorite;