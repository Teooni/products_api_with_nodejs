const NodeCache = require('node-cache');

class Cache {

    constructor(ttlSeconds) {
        this.cache = new NodeCache({ stdTTL: ttlSeconds, checkperiod: ttlSeconds * 0.2, useClones: false });
    }

    has(key) {
        return this.cache.has(key);
    }

    set(key, val) {
        this.cache.set(key, val);
    }

    /**
     * 
     * @param  listKey  Key of tha list that will be uppdated;
     * @param  valToBeAdded value that will be added to list 
     */
    addToList(listKey, valToBeAdded) {
        if (this.cache.has(`${listKey}`)) {
            let listOfObjects = [];
            listOfObjects = this.cache.get(`${listKey}`);
            listOfObjects.push(valToBeAdded);
            this.cache.set(`${listKey}`, listOfObjects);
        }
    }
    /**
     * This method wiil update the object with ObjectKey  for list and individual object with 
     * actual value.
     * @param {*} listKey - List of objects.
     * @param {*} ObjectKey - key of the object that will be updated
     * @param {*} actualValue - value to be used for update.
     */
    update(listKey, ObjectKey, actualValue) {
        let value = null;
        if (this.cache.has(`${ObjectKey}`)) {
            value = this.cache.get(`${ObjectKey}`);
            this.cache.set(`${ObjectKey}`, actualValue);
        }
        this.cache.del(`${listKey}`);
    }
    get(key) {
        return this.cache.get(key);
    }
    del(keys) {
        this.cache.del(keys);
    }

    delListAndObject(listKey, ObjectKey,) {
        this.cache.del(`${ObjectKey}`);
        this.cache.del(`${listKey}`);
    }
}

module.exports = Cache;