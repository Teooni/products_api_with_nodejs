
const AdministrateClient = require('../nivelAccesDate/AdministrateClient');
const AdministrateProdus = require('../nivelAccesDate/AdministrateProdus');
const AdministrateFavorite = require('../nivelAccesDate/AdministrateFavorite');

class StocareFactory {
    GetTipStocare(table_type) {
        if (table_type instanceof AdministrateClient) {
            console.log("AdministrateClient");
            return new AdministrateClient();
        }
        else if (table_type instanceof AdministrateProdus) {
            console.log("AdministrateProdus");
            return new AdministrateProdus();
        } else {
            console.log("AdministrateFavorite");
            return new AdministrateFavorite();
        }
    }
};

module.exports = StocareFactory;