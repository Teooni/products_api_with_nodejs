class Client{
    idclient;
    firstName;
    lastName;
    phone;
    email;
    password;
    isAdmin;
  

    constructor(idclient, firstName, lastName, phone, email, password, isAdmin){
        this.idclient = idclient;
        this.firstName=firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.email = email;
        this.password = password;
        this.isAdmin = isAdmin;
      
    }
}
module.exports = Client;