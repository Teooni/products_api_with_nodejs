const services = require('../services/services');

// select function is used to get all the favprites products from DB (using GetFavorites function from nivelAccesDate)
// and to put them into favoriteList view
exports.select = async function (req, res) {
  res.json({ favorites: await new services().GetFavorites(req.params.id) });
};
// insert method is used to insert in DB a favorite product then redirect to the favourites list
exports.add = async function (req, res) {
  res.json({ favorites: await new services().AddFavorit(req.body) });
};
// delete function is used to remove a favorite product, then redirect again to the favorites list view
exports.delete = async function (req, res) {
  res.json({ favorites: await new services().RemoveFavorit(req.params.idprodus, req.params.idclient) });
};
//---------------------------------------------------------------------------//
