const services = require('../services/services');
//--------------------------------- --- --------------------------------------//
exports.login = async function (req, res) {
  let [err, response] = await new services().Login(req.body.email);
  if (err === "SUCCESS") {
    return res.status(201).json(response);
  }
  // error
  return res.json(response);
}
//--------------------------------- SELECT ---------------------------------------//
exports.select = async function (req, res) {
  res.json({ clients: await new services().GetClients() });
};
//--------------------------------- --- --------------------------------------//
exports.selectOne = async function (req, res) {
  res.json({ clients: await new services().GetClient(req.params.i) });
};
//---------------------------------INSERT----------------------------------------//
exports.add = async function (req, res) {
  res.json({ clients: await new services().AddClient(req.body) });
};
//---------------------------------- UPDATE ---------------------------------------//
exports.update = async function (req, res) {
  res.json({ clients: await new services().UpdateClient(req.body) });
};
//------------------------------- DELETE -----------------------------------------//
exports.delete = async function (req, res) {
  res.json({ clients: await new services().RemoveClient(req.params.i) });
};
//---------------------------------------------------------------------------//
