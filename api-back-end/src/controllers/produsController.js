const services = require('../services/services');

exports.select = async function (req, res) {
  res.json({ products: await new services().GetProduse() });
};

exports.selectOne = async function (req, res) {
  res.json({ products: await new services().GetProdus(req.params.id) });
};

exports.add = async function (req, res) {
  res.json({ products: await new services().AddProdus(req.body) });
};

exports.update = async function (req, res) {
  res.json({ products: await new services().UpdateProdus(req.body) });
};
// delete function is used to remove a product, then redirect again to the product list view
exports.delete = async function (req, res) {
  res.json({ products: await new services().RemoveProdus(req.params.id) });
};
