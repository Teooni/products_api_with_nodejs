const AdministrateClient = require('../nivelAccesDate/AdministrateClient');
const AdministrateProdus = require('../nivelAccesDate/AdministrateProdus');
const AdministrateFavorite = require('../nivelAccesDate/AdministrateFavorite');
const StocareFactory = require('../utils/StocareFactory');
const myConstant = require('../utils/constants');
const CacheService = require('../utils/cache');

/**
 * Time tto live
 */
const ttl = 60 * 60 * 1; // cache for 1 Hour
/**
 * This is outside of class because we need to have one instance of this object
 */
const Cache = new CacheService(ttl); // Create a new cache service instance
//---------------------------------------------------------------------------//
class Configure {
    /**
     * used to get data's for users
     */
    Clienti;

    /**
    * used to get data's for products
    */
    Products;

    /**
    * used to get data's for favorites
    */
    Favorites;

    //---------------------------------------------------------------------------//
    constructor() {
        this.Clienti = new StocareFactory().GetTipStocare(new AdministrateClient());
        this.Products = new StocareFactory().GetTipStocare(new AdministrateProdus());
        this.Favorites = new StocareFactory().GetTipStocare(new AdministrateFavorite());
    }

    //-----------------------------------USER SERVICES --------------------------//
    async Login(email) {
        let resmysql = await this.Clienti.Login(email);
        try {
            if (resmysql[0].email) {
                console.log("succes");
                const token = jwt.sign(
                    {
                        email: resmysql[0].email,
                    },
                    "secret",
                    {
                        expiresIn: "72h",
                    }
                );

                var decoded = jwt.verify(token, 'secret');
                console.log(decoded);

                return ["SUCCESS", {
                    message: "Auth successfuly",
                    token: token,
                }];
            }
        } catch (error) {
            console.log('Nu exista');
            return ["ERROR", { error }];
        }
    }
    //---------------------------------------------------------------------------//
    async GetClients() {
        let response = undefined;
        if (Cache.has(myConstant.user_list)) {
            console.log("GET USERS FRON CACHE!");
            response = Cache.get(myConstant.user_list);
        }
        else {
            response = await this.Clienti.GetClients();
            Cache.set(myConstant.user_list, response);
        }
        return response;
    };
    //---------------------------------------------------------------------------//
    async GetClient(id) {
        let response = undefined;
        if (Cache.has(`${myConstant.user_}${id}`)) {
            response = Cache.get(`${myConstant.user_}${id}`);
        }
        else {
            response = await this.Clienti.GetClient(id);
            Cache.set(`${myConstant.user_}${id}`, response);
        }
        return response;
    }
    //---------------------------------------------------------------------------//
    async AddClient(user) {
        const response = await this.Clienti.AddClient(user);
        Cache.addToList(myConstant.user_list, response);
        return response;
    }
    //---------------------------------------------------------------------------//
    async UpdateClient(user) {
        const response = await this.Clienti.UpdateClient(user);
        Cache.update(myConstant.user_list, `${myConstant.user_}${user.idclient}`, response);
        return response;
    }
    //---------------------------------------------------------------------------//
    async RemoveClient(id) {
        const response = await this.Clienti.RemoveClient(id);
        Cache.delListAndObject(myConstant.user_list, `${myConstant.user_}${id}`);
        return response;
    }
    //------------------------------ END USER SERVICES --------------------------//
    //-------------------------------Products SERVICES --------------------------//
    async GetProduse() {
        let response = undefined;
        if (Cache.has(myConstant.product_list)) {
            console.log("GET products from cache!")
            response = Cache.get(myConstant.product_list);
        }
        else {
            response = await this.Products.GetProduse();
            Cache.set(myConstant.product_list, response);
        }
        return response;
    };
    //---------------------------------------------------------------------------//
    async GetProdus(id) {
        let response = undefined;
        if (Cache.has(`${myConstant.product_}${id}`)) {
            response = Cache.get(`${myConstant.product_}${id}`);
        }
        else {
            response = await this.Products.GetProdus(id);
            Cache.set(`${myConstant.product_}${id}`, response);
        }
        return response;
    }
    //---------------------------------------------------------------------------//
    async AddProdus(produs) {
        const response = await this.Products.AddProdus(produs);
        Cache.addToList(myConstant.product_list, response);
        return response;
    }
    //---------------------------------------------------------------------------//
    async UpdateProdus(produs) {
        const response = await this.Products.UpdateProdus(produs);
        Cache.update(myConstant.product_list, `${myConstant.product_}${produs.idprodus}`, response);
        return response;
    }
    //---------------------------------------------------------------------------//
    async RemoveProdus(id) {
        const response = await this.Products.RemoveProdus(id);
        Cache.delListAndObject(myConstant.product_list, `${myConstant.product_}${id}`);
        return response;
    }
    //----------------------------END Products SERVICES --------------------------//
    //-----------------------------------WEIGHT SERVICES -------------------------//
    async GetFavorites(idclient) {
        let response = undefined;
        if (Cache.has(`${myConstant.favorite_list}_${idclient}`)) {
            console.log("GET FAV FRON CACHE!");
            response = Cache.get(`${myConstant.favorite_list}_${idclient}`);
        }
        else {
            response = await this.Favorites.GetFavorites(idclient);
            Cache.set(`${myConstant.favorite_list}_${idclient}`, response);
        }
        return response;
    };
    //---------------------------------------------------------------------------//
    async GetFavorit(id) {
        let response = undefined;
        if (Cache.has(`${myConstant.favorite_}${id}`)) {
            response = Cache.get(`${myConstant.favorite_}${id}`);
        }
        else {
            response = await this.Favorites.GetFavorit(id)
            Cache.set(`${myConstant.favorite_}${id}`, response);
        }
        return response;
    }
    //---------------------------------------------------------------------------//
    async AddFavorit(favourit) {
        const response = await this.Favorites.AddFavorit(favourit);
        Cache.del(`${myConstant.favorite_list}_${favourit.idclient}`);
        return response;

    }
    //---------------------------------------------------------------------------//
    async RemoveFavorit(idprodus, idclient) {
        const response = await this.Favorites.RemoveFavorit(idprodus, idclient);
        Cache.delListAndObject(`${myConstant.favorite_list}_${idclient}`, `${myConstant.favorite_}${idclient}_${idprodus}`);
        return response;
    }
    //------------------------------ END WEIGHT SERVICES ------------------------//
};

module.exports = Configure;