/***
+----------+------+------+-----+---------+-------+
| Field    | Type | Null | Key | Default | Extra |
+----------+------+------+-----+---------+-------+
| idclient | int  | NO   | PRI | NULL    |       |
| idprodus | int  | NO   | PRI | NULL    |       |
+----------+------+------+-----+---------+-------+
 */
module.exports = (sequelize, DataTypes) => {
    const Fav = sequelize.define("favourites", {
        idfavorit: {
            type: DataTypes.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        idclient: {
            type: DataTypes.INTEGER,
            allowNull: false,
            unique: 'unique_idclient_idprodus'
        },
        idprodus: {
            type: DataTypes.INTEGER,
            allowNull: false,
            unique: 'unique_idclient_idprodus'
        }

    });

    // used for foreignKey
    Fav.associate = function (models) {
        Fav.belongsTo(models.clienti, { foreignKey: 'idclient', onDelete: 'CASCADE' });
        Fav.belongsTo(models.produs, { foreignKey: 'idprodus', onDelete: 'CASCADE' });
    }

    return Fav;
}