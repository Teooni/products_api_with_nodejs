/***
+-----------+-------------+------+-----+---------+----------------+
| Field     | Type        | Null | Key | Default | Extra          |
+-----------+-------------+------+-----+---------+----------------+
| idprodus  | int         | NO   | PRI | NULL    | auto_increment |
| name      | varchar(45) | NO   | UNI | NULL    |                |
| price     | float       | NO   |     | NULL    |                |
| isDeleted | varchar(45) | YES  |     | 0       |                |
+-----------+-------------+------+-----+---------+----------------+
 */
module.exports = (sequelize, DataTypes) => {
    const Produs = sequelize.define("produs", {
        idprodus: {
            type: DataTypes.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING(45),
            allowNull: false,
            unique: 'unique_name'
        },
        price: {
            type: DataTypes.FLOAT,
            allowNull: false
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    });

    // used for foreignKey
    Produs.associate = function (models) {
        Produs.hasMany(models.favourites, { foreignKey: 'idprodus' });
    }
    return Produs;
}