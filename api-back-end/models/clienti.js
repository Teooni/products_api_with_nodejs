/***
+-----------+-------------+------+-----+---------+----------------+
| Field     | Type        | Null | Key | Default | Extra          |
+-----------+-------------+------+-----+---------+----------------+
| idclient  | int         | NO   | PRI | NULL    | auto_increment |
| firstName | varchar(45) | YES  |     | NULL    |                |
| lastName  | varchar(45) | YES  |     | NULL    |                |
| phone     | varchar(45) | YES  |     | NULL    |                |
| email     | varchar(45) | NO   | UNI | NULL    |                |
| password  | varchar(45) | NO   |     | NULL    |                |
| isAdmin   | varchar(45) | YES  |     | 0       |                |
| isDeleted | varchar(45) | YES  |     | 0       |                |
+-----------+-------------+------+-----+---------+----------------+
 */
module.exports = (sequelize, DataTypes) => {
    const Client = sequelize.define("clienti", {
        idclient: {
            type: DataTypes.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        firstName: {
            type: DataTypes.STRING(45),
            allowNull: true
        },
        lastName: {
            type: DataTypes.STRING(45),
            allowNull: true
        },
        phone: {
            type: DataTypes.STRING(45),
            allowNull: true
        },
        email: {
            type: DataTypes.STRING(45),
            allowNull: false,
            unique: 'unique_email'
        },
        password: {
            type: DataTypes.STRING(45),
            allowNull: false
        },
        isAdmin: {
            type: DataTypes.STRING(1),
            allowNull: false,
            defaultValue: '0',
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    });

    // used for foreignKey
    Client.associate = function (models) {
        Client.hasMany(models.favourites, { foreignKey: 'idclient' });
    }
    return Client;
}