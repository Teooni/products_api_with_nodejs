import React from "react";
import Title from "../../components/Title/Title";
import Products from "../../components/Products/Products";
import "./Home.css";
import { useNavigate } from "react-router";
import { setAdmin } from "../../redux/actions";
import { useDispatch } from "react-redux";
import { Button } from "react-bootstrap";

const Home = ({
  products,
  clients,
  favorites,
  isAdmin,
  currentProductId,
  showAddProduct,
}) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const handleLogout = () => {
    localStorage.removeItem("loggedEmail");
    localStorage.removeItem("idClient");
    navigate("/login");
    dispatch(setAdmin(false));
  };

  return (
    <div className="home">
      <Title title="Welcome to our page!" />

      {!isAdmin && (<Button variant="secondary" onClick={() => navigate("/favorite")}  >
        Go to favorites page
      </Button>)}

      <Button variant="dark" onClick={handleLogout} className="logout" >
        Logout
      </Button>
      <Products
        products={products}
        clients={clients}
        favorites={favorites}
        isAdmin={isAdmin}
        currentProductId={currentProductId}
        showAddProduct={showAddProduct}
      />
    </div>
  );
};

export default Home;
