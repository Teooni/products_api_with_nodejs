import React, { useState } from "react";
import { addItem } from "../../common/api";
import { dbPaths } from "../../common/constants";
import "./Register.css";
import { useNavigate } from "react-router";

const Register = () => {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [password, setPassword] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [email, setEmail] = useState("");
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);

  const navigate = useNavigate();

  const handleRegister = () => {
    if (
      firstName.length < 1 ||
      lastName.length < 1 ||
      phoneNumber.length < 1 ||
      password.length < 1 ||
      email.length < 1
    ) return;
    setLoading(true);
    const obj = {
      firstName,
      lastName,
      phone: phoneNumber,
      email,
      password,
      isAdmin: "0",
    };
    addItem(dbPaths[1], obj)
      .then((response) => {
        if (response) {
          navigate("/login");
          window.location.reload();
        }
      })
      .catch((error) => {
        console.log(error);
        setError(error);
      })
      .finally(() => setLoading(false));
  };

  return (
    <div className="register-container">
      <div className="register">
        <p className="register__title">REGISTER</p>
        <div className="register__form">
          <div className="register__form-group">
            <label htmlFor="firstName">First name</label>
            <input
              id="firstName"
              type="text"
              name="firstName"
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
            />
          </div>
          <div className="register__form-group">
            <label htmlFor="lastName">Last name</label>
            <input
              id="lastName"
              type="text"
              name="lastName"
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
            />
          </div>
          <div className="register__form-group">
            <label htmlFor="email">Email</label>
            <input
              id="email"
              type="email"
              name="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className="register__form-group">
            <label htmlFor="password">Password</label>
            <input
              id="password"
              type="password"
              name="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <div className="register__form-group">
            <label htmlFor="texphoneNumbert">Phone number</label>
            <input
              id="phoneNumber"
              type="text"
              name="phoneNumber"
              value={phoneNumber}
              onChange={(e) => setPhoneNumber(e.target.value)}
            />
          </div>
        </div>
        <div className="register__button" onClick={handleRegister}>
          Register
        </div>
        <p className="login-button" onClick={() => navigate("./login")}>
          Go to Login
        </p>
        {loading && <div className="register__loading">Loading...</div>}
        {error && <div className="register__error">Something went wrong</div>}
      </div>
    </div>
  );
};

export default Register;
