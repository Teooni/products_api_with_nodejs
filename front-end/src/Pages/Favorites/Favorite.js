import React from "react";
import Title from "../../components/Title/Title";
import "./Favorite.css";
import { useNavigate } from "react-router";
import { useDispatch } from "react-redux";
import { Button } from "react-bootstrap";
import Favorites from "../../components/Favorites/Favorites";

const Home = ({
  products,
  clients,
  favorites,
  isAdmin,
  currentProductId,
  showAddProduct,
}) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  return (
    <>
      <Title title="Favorites list" />
      <Button variant="secondary" onClick={() => navigate("/")}  >
        &lt; Go to products page
      </Button>

      <Favorites
        products={products}
        clients={clients}
        favorites={favorites}
        isAdmin={isAdmin}
        currentProductId={currentProductId}
        showAddProduct={showAddProduct}
      />
    </>
  );
};

export default Home;
