import React, { useState } from "react";
import { useNavigate } from "react-router";
import "./Login.css";

const Login = ({ clients }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(false);

  const navigate = useNavigate();
  const handleLogin = () => {
    console.log({ email, password, clients });
    setError(false);
    const myclient = clients.find(
      (client) => client.email === email && client.password === password
    );
    if (myclient) {
      localStorage.setItem("loggedEmail", email);
      localStorage.setItem("idClient", myclient.idclient);
      navigate("/");
    } else {
      setError(true);
    }
  };

  return (
    <div className="login-container">
      <div className="login">
        <p className="login__title">LOGIN</p>
        <div className="login__form">
          <div className="login__form-group">
            <label htmlFor="email">Email</label>
            <input
              id="email"
              type="email"
              name="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className="login__form-group">
            <label htmlFor="password">Password</label>
            <input
              id="password"
              type="password"
              name="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
        </div>
        <div className="login__button" onClick={handleLogin}>
          Login
        </div>
        <p className="register-button" onClick={() => navigate("/register")}>
          Go to Register
        </p>
        {error && <div className="login__error">Something went wrong</div>}
      </div>
    </div>
  );
};

export default Login;
