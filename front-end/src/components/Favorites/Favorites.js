import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Table } from 'react-bootstrap';
import { dbPaths } from "../../common/constants";
import { deleteMultipleItems } from "../../common/api";
import { FaTrashAlt } from 'react-icons/fa';
import "./Favorites.css";

const Favorites = ({
  products,
  clients,
  favorites,
  isAdmin,
  showAddProduct,
  currentProductId,
}) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const dispatch = useDispatch();

  const handleDelete = (idclient, idprodus) => {
    setLoading(true);
    setError(false);
    deleteMultipleItems(dbPaths[2], idclient, idprodus)
      .then((response) => {
        response && window.location.reload();
      })
      .catch((error) => {
        console.log(error);
        setError(error);
      })
      .finally(() => setLoading(false));
  };

  return (
    <>
      {!isAdmin && (<div>
        <hr />

        <hr />
        <Table striped bordered hover variant="dark" style={{ width: "50%", margin: "auto" }}>
          <thead>
            <tr>
              <th>Nr.crt</th>
              <th>Name</th>
              <th>Price</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {favorites.map((fav, index) => (
              <tr key={fav.idprodus}>
                <td>{index + 1}</td>
                <td>{fav?.produ?.name}</td>
                <td>{fav?.produ?.price}</td>
                <td style={{ cursor: 'pointer' }}> <span onClick={() =>
                  handleDelete(fav.idclient, fav.idprodus)} style={{ marginRight: "20px" }}> <FaTrashAlt /></span></td>
              </tr>

            ))}
          </tbody>
        </Table>
        <hr />
      </div>)}
    </>

  );
};

export default Favorites;
