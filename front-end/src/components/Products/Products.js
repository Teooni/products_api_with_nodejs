import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Table, Modal, Button } from 'react-bootstrap';
import { addItem, editItem } from "../../common/api";
import { dbPaths } from "../../common/constants";
import { getLoggedEmail } from "../../common/helpers";
import { setAdmin, setProductId, setShowAdd } from "../../redux/actions";
import Product from "../Product/Product";
import Title from "../Title/Title";
import "./Products.css";

const Products = ({
  products,
  clients,
  favorites,
  isAdmin,
  showAddProduct,
  currentProductId,
}) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [currentName, setCurrentName] = useState("");
  const [favorite, setFavorite] = useState("");
  const [currentPrice, setCurrentPrice] = useState(1);
  const dispatch = useDispatch();

  useEffect(() => {
    if (currentProductId) {
      const currentProduct = products.find(
        (product) => product.idprodus === currentProductId
      );
      if (currentProduct) {
        setCurrentName(currentProduct.name);
        setCurrentPrice(currentProduct.price);
      }
    } else {
      setCurrentName("");
      setCurrentPrice(1);
    }
    return () => {
      setCurrentName("");
      setCurrentPrice(1);
    };
  }, [products, currentProductId]);

  useEffect(() => {
    const currentEmail = getLoggedEmail();
    clients &&
      clients.length > 0 &&
      clients.find((client) => client.email === currentEmail)?.isAdmin == "1" &&
      dispatch(setAdmin(true));
  }, []);

  const handleSubmit = () => {
    setLoading(true);
    setError(false);
    if (showAddProduct) {
      console.log("ADD", { name: currentName, price: currentPrice });
      addItem(dbPaths[0], { name: currentName, price: currentPrice })
        .then((response) => {
          response && window.location.reload();
        })
        .catch((error) => {
          console.log(error);
          setError(error);
        })
        .finally(() => setLoading(false));
    } else {
      console.log("EDIT", { name: currentName, price: currentPrice });
      editItem(dbPaths[0], currentProductId, {
        idprodus: currentProductId,
        name: currentName,
        price: currentPrice,
      })
        .then((response) => {
          response && window.location.reload();
        })
        .catch((error) => {
          console.log(error);
          setError(error);
        })
        .finally(() => setLoading(false));
    }
  };

  return (
    <>
      <Title title="Products" />
      {isAdmin && (
        <div
          className="button"
          onClick={() => {
            dispatch(setShowAdd(true));
            dispatch(setProductId(null));
          }}
        >
          Add product
        </div>
      )}

      <div className="products">
        {products.map((product) => (
          <Product
            key={product.idprodus}
            idprodus={product.idprodus}
            name={product.name}
            price={product.price}
            isAdmin={isAdmin}
            favorites={favorites}
          />
        ))}
      </div>
      {currentProductId || showAddProduct ? (
        <div className="edit-product">
          <div className="form-group">
            <label htmlFor="name">Product name</label>
            <input
              id="name"
              type="text"
              name="name"
              value={currentName}
              onChange={(e) => setCurrentName(e.target.value)}
            />
          </div>
          <div className="form-group">
            <label htmlFor="price">Product price (euro)</label>
            <input
              id="price"
              type="text"
              name="price"
              value={currentPrice}
              onChange={(e) => setCurrentPrice(+e.target.value)}
            />
          </div>
          <div className="button" onClick={handleSubmit}>
            Submit
          </div>
          {loading && <p>Loading...</p>}
          {error && <p>Something went wrong</p>}
          <span
            onClick={() => {
              dispatch(setShowAdd(false));
              dispatch(setProductId(null));
            }}
          >
            x
          </span>
        </div>
      ) : null}
    </>

  );
};

export default Products;
