import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { addItem, deleteItem, deleteMultipleItems } from "../../common/api";
import { dbPaths } from "../../common/constants";
import { setProductId, setShowAdd } from "../../redux/actions";
import "./Product.css";
import { BsHeart } from "react-icons/bs";
import { BsHeartFill } from "react-icons/bs";
import { getClientID } from "../../common/helpers";

const Product = ({ isAdmin, name, price, idprodus, favorites }) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [isfavorite, setIsFavorite] = useState(false);
  const [heart, setHeart] = useState(<BsHeart />);
  const dispatch = useDispatch();

  useEffect(() => {
    console.log("useEffect Product chack fav!")
    if (favorites) {
      console.log(favorites);
      const currentProduct = favorites.find(
        (fav) => fav.idprodus == idprodus && fav.idclient == getClientID()
      );
      console.log("FAV PRODUCT", currentProduct)
      if (currentProduct) {
        setIsFavorite(true);
        setHeart(<BsHeartFill />);
      }
    } else {
      setIsFavorite(false);
      setHeart(<BsHeart />);
    }
  }, [favorites]);

  const handleDelete = () => {
    console.log('DELETE product', idprodus)
    setLoading(true);
    setError(false);
    deleteItem(dbPaths[0], idprodus)
      .then((response) => {
        response && window.location.reload();
      })
      .catch((error) => {
        console.log(error);
        setError(error);
      })
      .finally(() => setLoading(false));
  };
  const handleFavorite = () => {
    const idClient = getClientID();
    console.log("idprodus & idclient", idprodus, idClient)
    if (!isfavorite) {
      const fav = { idprodus: idprodus, idclient: idClient };
      console.log("ADD Fav!", fav);

      addItem(dbPaths[2], fav)
        .then((response) => {
          response && window.location.reload();
        })
        .catch((error) => {
          console.log("ADD FAV!", error);
          setError(error);
        })
        .finally(() => setLoading(false));
    }
    else {
      setLoading(true);
      setError(false);
      deleteMultipleItems(dbPaths[2], idClient, idprodus)
        .then((response) => {
          response && window.location.reload();
        })
        .catch((error) => {
          console.log(error);
          setError(error);
        })
        .finally(() => setLoading(false));
    }
  };
  return (
    <div className="product">
      <p className="product__title">{name}</p>
      <p className="product__price">{price} euro</p>
      {isAdmin && (
        <>
          <div
            className="product__edit"
            onClick={() => {
              dispatch(setProductId(idprodus));
              dispatch(setShowAdd(false));
            }}
          >
            Edit
          </div>
          {loading && <p>Loading...</p>}
          {error && <p>Something went wrong</p>}
          <span onClick={handleDelete}>X</span>
        </>
      )}
      {!isAdmin && <span onClick={handleFavorite}>{heart}</span>}
    </div>
  );
};

export default Product;
