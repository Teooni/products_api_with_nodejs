import React from "react";
import { Navigate } from "react-router-dom";
import { getLoggedEmail } from "../../common/helpers";

const PrivateRoute = ({ children }) => {
  return getLoggedEmail() ? children : <Navigate to="/login" />;
};

export default PrivateRoute;
