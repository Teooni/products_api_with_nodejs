export const reduxActions = {
  SET_DATA: "SET_DATA",
  SET_CURRENT_PRODUCT_ID: "SET_CURRENT_PRODUCT_ID",
  SET_ADMIN: "SET_ADMIN",
  SET_SHOW_ADD: "SET_SHOW_ADD",
};

export const dbPaths = ["products", "clients","favorites"];

//http://localhost:3000/posts/1

export const backendBaseUrl = "http://localhost:3003";
