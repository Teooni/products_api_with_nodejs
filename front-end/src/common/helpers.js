export const getLoggedEmail = () => {
  return localStorage.getItem("loggedEmail") ?? "";
};
export const getClientID = () => {
  return localStorage.getItem("idClient") ?? "";
};