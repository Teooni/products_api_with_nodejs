import { reduxActions } from "../common/constants";

export const setData = (prop, value) => ({
  type: reduxActions.SET_DATA,
  prop,
  value,
});

export const setProductId = (productId) => ({
  type: reduxActions.SET_CURRENT_PRODUCT_ID,
  productId,
});

export const setAdmin = (value) => ({
  type: reduxActions.SET_ADMIN,
  value,
});

export const setShowAdd = (value) => ({
  type: reduxActions.SET_SHOW_ADD,
  value,
});
