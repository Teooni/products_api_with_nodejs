import { reduxActions } from "../common/constants";

const initialState = {
  clients: [],
  products: [],
  favorites: [],
  currentProductId: null,
  isAdmin: false,
  showAddProduct: false,
};

export const dataReducer = (state = initialState, action) => {
  switch (action.type) {
    case reduxActions.SET_DATA:
      return {
        ...state,
        [action.prop]: action.value,
      };

    case reduxActions.SET_CURRENT_PRODUCT_ID:
      return {
        ...state,
        currentProductId: action.productId,
      };

    case reduxActions.SET_ADMIN:
      return {
        ...state,
        isAdmin: action.value,
      };
    case reduxActions.SET_SHOW_ADD:
      return {
        ...state,
        showAddProduct: action.value,
      };
    default:
      return state;
  }
};
