import "./App.css";
import { useSelector, useDispatch } from "react-redux";
import { Routes, Route, Navigate } from "react-router-dom";
import Home from "./Pages/Home/Home";
import Login from "./Pages/Login/Login";
import Register from "./Pages/Register/Register";
import { useEffect, useState } from "react";
import { getItems } from "./common/api";
import { setData } from "./redux/actions";
import PrivateRoute from "./components/PrivateRoute/PrivateRoute";
import { dbPaths } from "./common/constants";
import { getLoggedEmail, getClientID } from "./common/helpers";
import Favorites from "./Pages/Favorites/Favorite";

function App() {
  const { clients, products, favorites, currentProductId, isAdmin, showAddProduct } =
    useSelector((state) => state);
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);

  useEffect(() => {
    setLoading(true);
    dbPaths.forEach((path, index, paths) => {
      if (dbPaths.length - 1 === index) {
        setLoading(false);
      }
      else {
        getItems(path)
          .then((response) => {
            console.log(response.data);
            if (response && response.data) {
              console.log('aici');
              dispatch(setData(path, response.data[path]));
            }
          })
          .catch((error) => {
            console.log("Eroare la preluare date", error);
            setError(error);
          })
          .finally(() => {
            index === paths.length - 1 && setLoading(false);
          });
      }
    });

    const clientID = getClientID();

    if (getLoggedEmail() !== "") {
      // This set Loading is required because we will send incomplet data to Components. 
      // Alse this fixed the problem for isAdmin toggle with normal account when we press refresh button
      setLoading(true);
      console.log("Load favorites because the client is auth!", clientID)
      getItems(dbPaths[2], clientID)
        .then((response) => {
          console.log(response.data);
          if (response && response.data) {
            console.log('aici');
            dispatch(setData(dbPaths[2], response.data[dbPaths[2]]));
          }
        })
        .catch((error) => {
          console.log(error);
          setError(error);
        })
        .finally(() => {
          setLoading(false);
        });
    }
  }, []);
  return (
    <div className="App">
      {loading ? (
        <h1>Loading...</h1>
      ) : (
        <Routes>
          <Route
            exact
            path="/"
            element={
              <PrivateRoute>
                <Home
                  products={products}
                  clients={clients}
                  favorites={favorites}
                  isAdmin={isAdmin}
                  currentProductId={currentProductId}
                  showAddProduct={showAddProduct}
                />
              </PrivateRoute>
            }
          />
          <Route
            exact
            path="/favorite"
            element={
              <PrivateRoute>
                <Favorites
                  products={products}
                  clients={clients}
                  favorites={favorites}
                  isAdmin={isAdmin}
                  currentProductId={currentProductId}
                  showAddProduct={showAddProduct}
                />
              </PrivateRoute>
            }
          />
          <Route exact path="/login" element={<Login clients={clients} />} />
          <Route exact path="/register" element={<Register />} />
          <Route path="*" element={<Navigate from="*" to="/" />} />
        </Routes>
      )}
      {error && <h1>Erro: {"" + error}</h1>}
    </div>
  );
}

export default App;
